<?php

namespace Fd\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FdFrontendBundle:Default:index.html.twig');
    }
}
