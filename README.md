Symfony3 Docker Template
========================

__Author:__ Foued Dghaies <foued.dghaies@lidl.com>

```
echo "127.0.0.1 sulu.dev www.sulu.dev >> /etc/hosts"
```
## Docker 
```
docker-compose build
docker-compose up -d
docker exec -it sulu_php-fpm /bin/bash

```
### use docker-sync for performance optimization
Documentation: https://github.com/EugenMayer/docker-sync/wiki
```
# install unison or rsync
brew install unison  
brew install rsync

# install fswatch to check for file changes
brew install fswatch

# install docker-sync
gem install docker-sync
```
####start docker sync
```
docker-sync start
docker-compose up -d
#OR
docker-sync-stack start

#after every stop
docker-sync clean

```



- (only initial or after changes) docker-compose build - __build your images__

- docker-compose up -d - __run your services__

- docker-compose stop - __stop your services__

- docker exec -it sulu_php-fpm /bin/bash - __connect to service__

- (warning) docker stop $(docker ps -a -q) - __stop ALL of Docker containers__

- (warning) docker rm -f $(docker ps -a -q) - __remove ALL of Docker containers__

- (warning) docker rmi -f $(docker images -a -q) - __remove ALL of Docker images__

## Browser url

- http://sulu.dev:8080


## Ant commands

- ant __full-build__ - Performs static analysis, runs the tests, and generates project documentation

- ant __full-build-parallel__ - Performs static analysis (executing the tools in parallel), runs the tests, and generates project documentation

- ant __quick-build__ - Performs a lint check and runs the tests (without generating code coverage reports)
  
- ant __dev-build__ - Run unit tests with PHPUnit &amp; Aggregate tool output with PHP CodeBrowser
  
- ant __static-analysis__ - Performs static analysis
  
- ant __static-analysis-parallel__ - Performs static analysis (executing the tools in parallel)

